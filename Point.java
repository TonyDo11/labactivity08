import java.util.ArrayList;

public class Point {
    public double x;
    public double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public boolean equals(Object other) {
       /*  this.x = other.x; "x cannot be resolved as a field" */

       if(!(other instanceof Point)) {
        return false;
       }
       Point otherPoint = (Point) other;
        return this.x == otherPoint.x && this.y == otherPoint.y;
    }



    public static void main(String[]args) {
        ArrayList<Point> points = new ArrayList<>();

        Point point1 = new Point(2.0, 3.5);
        Point point2 = new Point(1.0, 4.0);
        Point point3 = new Point(3.5, 2.0);
        points.add(point1);
        points.add(point2);
        points.add(point3);

        Point target = new Point(point2.x, point2.y);

        System.out.println(points.contains(target)); /* false */
    }
}
