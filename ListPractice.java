import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.List;
public class ListPractice {
    public int countUpperCase(String[] strings) {
        int count = 0;

        for (String str : strings) {
            if (isAllUpperCase(str)) {
                count++;
            }
        }

        return count;
    }

    public int countUpperCase(List<String> words) {
        int count = 0;

        for (String word : words) {
            if (isAllUpperCase(word)) {
                count++;
            }
        }

        return count;
    }

    public String[] getUpperCase(String[] strings) {
        int count = countUpperCase(strings);
        String[] uppercaseStrings = new String[count];
        int index = 0;

        for (String str : strings) {
            if (isAllUpperCase(str)) {
                uppercaseStrings[index] = str;
                index++;
            }
        }

        return uppercaseStrings;
    }

    public List<String> addUpperCaseToWords(String[] strings, List<String> words) {
        for (String str : strings) {
            if (isAllUpperCase(str)) {
                words.add(str);
            }
        }
        return words;
    }

    private boolean isAllUpperCase(String str) {
        String regex = "^[A-Z]+$";
        return Pattern.matches(regex, str);
    }

    public static void main(String[] args) {

        ListPractice listPractice = new ListPractice();

        String[] strings = {"ABC", "123", "DEF", "aBc", "XYZ"};
        int stringCount = listPractice.countUpperCase(strings);

         List<String> uppercaseWords = new ArrayList<>();
         listPractice.addUpperCaseToWords(strings, uppercaseWords);
        int arrayListCount = listPractice.countUpperCase(uppercaseWords);

        System.out.println("Number of uppercase strings in String[]: " + stringCount); /* should be 3 now */
        System.out.println("Number of uppercase words in ArrayList: " + arrayListCount); /* should be 3 now */
    }
}
